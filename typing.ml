open Lang;;

type environment = {localvars: vardecl list; funbind: fundecl list};;

(*1: Vérification du typage des expressions *)

(* Vérifier si une variable est présente dans l'environnement et renvoyer son type *)
let verif_var env var =
	let rec rush = function
		(Vardecl (tp, name) :: l) -> (function var -> if name = var then tp else rush (l) (var))
		| [] -> failwith "Absence de la variable spécifiée dans l'environnement!"
in rush (env.localvars) (var);;

(* Vérifier si un appel de fonction se trouve bien dans un environnement et si son paramétrage est correct *)
let rec verif_call_type env = function [] -> (function [] -> true | (f :: m) -> failwith "Il y a moins d'arguments fournis que de paramètres définis pour cette fonction!")
	| (expr :: l) -> (function (Vardecl (tp, n) :: m) when tp_expr (env) (expr) = tp -> verif_call_type (env) (l) (m)
		| (Vardecl (_, _) :: _) -> failwith "L'appel de variable n'a pas le bon type!"
		| [] -> failwith "Il y a plus d'arguments fournis que de paramètres définis pour cette fonction!")

and verif_func env name exprs =
	let rec rush = function (Fundecl (out, f, inn) :: l) ->
		(function (name, exprs) ->
			if name = f then
				if verif_call_type (env) (exprs) (inn) then out
				else failwith ("Fonction " ^ name ^ " appelée avec un mauvais typage de paramètres!")
			else rush (l) (name, exprs))
	| [] -> failwith ("Fonction " ^ name ^ " non trouvée dans le présent environnement!")
in rush (env.funbind) (name, exprs)

and tp_expr env expr =
let rec rush = function Const (IntV _) -> IntT | Const (FloatV _) -> FloatT
	| Const (BoolV _) -> BoolT | Const (StringV _) -> StringT | Const (LitV _) -> LitT
	| VarE var -> verif_var (env) (var)
	| BinOp (BInt _, e1, e2) -> if rush (e1) = IntT && rush (e2) = IntT then IntT else failwith "Un opérateur arithmétique entier n'accepte que les entiers!"
	| BinOp (BFloat _, e1, e2) -> if rush (e1) = FloatT && rush (e2) = FloatT then FloatT else failwith "Un opérateur arithmétique flottant n'accepte que les flottants!"
	| BinOp (BBool _, e1, e2) -> if rush (e1) = BoolT && rush (e2) = BoolT then BoolT else failwith "Une opération boolléenne n'accepte que les booléenss!"
	| BinOp (BCompar _, e1, e2) -> let t1 = rush (e1) and t2 = rush (e2) in
		if (t1 = IntT && t2 = IntT) || (t1 = FloatT && t2 = FloatT) || (t1 = StringT && t2 = StringT)
		then BoolT else failwith "Un opérateur de comparaison n'accepte que deux entiers, deux flottants ou deux chaînes de caractères!"
	| CondE (expr, e1, e2) -> let t1 = rush (e1) and t2 = rush (e2) in
		 if rush (expr) = BoolT && t1 = t2 then t1 else failwith "Types différents dans une expression conditionnelle!"
	| CallE (name, exprs) -> verif_func (env) (name) (exprs)
in rush (expr);;

(*2: Vérification du typage des commandes *)

(* Vérifier que deux commandes sont de même type *)
let tp_com env com =
let rec rush = function Exit -> VoidT
	| Skip -> VoidT
	| Assign (n1, e1) -> if verif_var (env) (n1) = tp_expr (env) (e1) then VoidT else failwith ("Mauvais assignement de la variable " ^ n1 ^ "!")
	| Seq (c1, c2) -> if rush (c1) = VoidT then rush (c2) else failwith "Return dans une séquence détecté!"
	| CondC (expr, c1, c2) -> let t1 = rush (c1) and t2 = rush (c2) in
		 if tp_expr (env) (expr) = BoolT && t1 = t2 then t1 else failwith "Types différents dans une commande conditionnelle!"
	| Loop c1 -> if rush (c1) = VoidT then VoidT else failwith "Return dans une boucle détecté!"
	| CallC (name, exprs) -> let _ = verif_func (env) (name) (exprs) in VoidT
	| Return e1 -> let t1 = tp_expr (env) (e1) in if t1 = VoidT then failwith "On ne peut pas retourner d'expression Void!" else t1
in rush (com);;

let tp_fundefn funs = function Fundefn (Fundecl (tp, f, l), com) -> let tc = tp_com ({localvars = l; funbind = funs}) (com) in
	if tp = tc then () else failwith "Problème de type de la commande!";;

let tp_prog = function Prog (decls, defs) -> List.iter (tp_fundefn decls) defs;;
