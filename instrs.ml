(* Datatypes for Postscript instructions *)


open Lang

(* I couldn't figure out a good way to have IVar of int.
   This would require a mark before the parameters, indexed from the bottom;
   but ssignements require an index from the top of the stack, or an array.
   Using a dict is actually easier. *)
type instr =
  IVal of value
| IVar of vname
| IOper of string
| IBloc of instr
| ISeq of instr list
| ILoop of instr
| IDef of fname * instr

let string_of_value = function
  BoolV v -> string_of_bool v
| FloatV v -> string_of_float v
| IntV v -> string_of_int v
| StringV v -> "(" ^ v ^ ")"
| LitV v -> "/" ^ v ^ " "

(* Every instruction MUST end with whitespace *)

let rec string_of_instr = function
    IVal v -> string_of_value v ^ " "
  | IVar v -> v ^ " "
  | IOper s -> s ^ " "
  | IBloc i -> "{ " ^ string_of_instr i ^ "} "
  | ISeq(i::l) -> string_of_instr i ^ string_of_instr (ISeq l)
  | ISeq [] -> ""
  | ILoop i -> string_of_instr(IBloc i) ^ "loop "
  | IDef(n, i) -> "/" ^ n ^ " " ^ string_of_instr(IBloc i) ^ "def "

