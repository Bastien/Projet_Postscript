/* Declaration of native Postscript procedures / functions */

void newpath ();
void moveto(float x, float y);
void rlineto(float x, float y);
void arc(float x, float y, float r, float a, float b);
void closepath();
void stroke ();

/* Function definition */

void circle(float x, float y, float r) {
  newpath();
  arc(x, y, r, 0., 360.);
  stroke();
}

void tunnel(int d, float x, float y, float r, float scale) {
  if (d != 0) {
    circle(x, y, r);
    tunnel(d-1, x, y, r *. scale, scale);
  }
}

void main () {
  tunnel(50, 300., 450., 250., 0.95);
}

