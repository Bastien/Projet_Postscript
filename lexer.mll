{
  open Lexing
  open Parser
  open Lang
  exception Lexerror

  let pos lexbuf = (lexeme_start lexbuf, lexeme_end lexbuf)

  let advance_line_pos pos =
    { pos with pos_lnum = pos.pos_lnum + 1; pos_bol = pos.pos_cnum; }

  let advance_line lexbuf =
    lexbuf.lex_curr_p <- advance_line_pos lexbuf.lex_curr_p

}

let includeline = '#' [^ '\n']* '\n'
let alph = ['a'-'z''A'-'Z']
let num = ['0'-'9']
let ident = alph(alph|num|'_')*
let literal = '/'alph(alph|'-')*
let comment = "/*" ([^'*']|'*'+[^'*''/'])*'*'+'/'

rule token = parse
| [' ' '\t']            {token lexbuf}    (* white space: recursive call of lexer *)
| '\n'                  {advance_line lexbuf; token lexbuf}    (* white space: recursive call of lexer *)
| includeline           {advance_line lexbuf; token lexbuf}    (* C include directives --> ignore *)
| comment               {token lexbuf}    (* comment --> ignore *)
| literal as l          {LITCONSTANT l}
| num+ as c             {INTCONSTANT(int_of_string c)}
| num+'.'num* as c      {FLOATCONSTANT(float_of_string c)}
| "true"                {BCONSTANT true}
| "false"               {BCONSTANT false}
| '"'                   {STRINGCONSTANT(string lexbuf)}    (* '"' breaks some editors formating, so here it is again *)
| '('                   {LPAREN}
| ')'                   {RPAREN}
| '{'                   {LBRACE}
| '}'                   {RBRACE}
| ';'                   {SEMICOLON}
| ','                   {COMMA}
| '='                   {EQ}
| '*'                   {TIMES}
| '/'                   {DIV}
| '%'                   {MOD}
| '+'                   {PLUS}
| '-'                   {MINUS}
| "*."                  {FTIMES}
| "/."                  {FDIV}
| "+."                  {FPLUS}
| "-."                  {FMINUS}
| '<'                   {BCLT}
| "<="                  {BCLE}
| '>'                   {BCGT}
| ">="                  {BCGE}
| "=="                  {BCEQ}
| "!="                  {BCNE}
| "&&"                  {BLAND}
| "||"                  {BLOR}
| '!'                   {NOT}
| '?'                   {QMARK}
| ':'                   {COLON}
| "if"                  {IF}
| "else"                {ELSE}
| "while"               {WHILE}
| "for"                 {FOR}
| "return"              {RETURN}
| "void"                {TP VoidT}
| "int"                 {TP IntT}
| "float"               {TP FloatT}
| "bool"                {TP BoolT}
| "string"              {TP StringT}
| eof                   {EOF}
| ident as i            {IDENTIFIER i}
| _                     {Printf.printf "ERROR: unrecogized symbol '%s'\n" (Lexing.lexeme lexbuf); raise Lexerror}

(* this takes over whenever a string begins; the whole string is unescaped and returned as a token *)
and string = parse
| '"'                   {""}
| [^'"''\\''\n']+ as s  {s ^ string lexbuf}
| "\\n"                 {"\n" ^ string lexbuf}
| "\\\""                {"\"" ^ string lexbuf}
| "\\\\"                {"\\" ^ string lexbuf}

and

ruleTail acc = parse
| eof {acc}
| _* as str {ruleTail (acc ^ str) lexbuf}
