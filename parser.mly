%{
open Lang
%}

%token <string> IDENTIFIER
%token <string> LITCONSTANT
%token <string> STRINGCONSTANT
%token <Lang.tp> TP
%token <bool> BCONSTANT
%token <int> INTCONSTANT
%token <float> FLOATCONSTANT
%token PLUS MINUS TIMES DIV MOD FPLUS FMINUS FTIMES FDIV
%token LPAREN RPAREN LBRACE RBRACE
%token EQ COMMA SEMICOLON COLON QMARK
%token IF ELSE WHILE FOR RETURN NOT BCEQ BCGE BCGT BCLE BCLT BCNE BLAND BLOR
%token EOF

%right IF ELSE
%right QMARK COLON
/* The ternary operator can be thought of as a binary operator between the condition and alternative;
   the consequent is unambiguously bounded. */
%left BLOR
%left BLAND
%left BCEQ BCNE
%left BCLT BCLE BCGT BCGE
%left PLUS MINUS FPLUS FMINUS
%left TIMES DIV MOD FTIMES FDIV

%start start
%type <Lang.prog> start

%%

start:
  fundefn start
  { match $1, $2 with Fundefn(d, _), Prog(decl, defn) -> Prog(d::decl, $1::defn) }
| fundecl SEMICOLON start
  { match $3 with Prog(decl, defn) -> Prog($1::decl, defn) }
| EOF
  { Prog([], []) }
;

fundefn:
  fundecl LBRACE block_item_list_opt RBRACE
  { Fundefn($1, $3) }
;

fundecl:
  TP IDENTIFIER LPAREN vardecl_comma_list_opt RPAREN
  { Fundecl($1, $2, $4) }
;

vardecl_comma_list_opt:
  /* empty */
  { [] }
| vardecl
  { [$1] }
| vardecl COMMA vardecl_comma_list_opt
  { $1::$3 }
;

vardecl:
  TP IDENTIFIER
  { Vardecl($1, $2) }
;

block_item_list_opt:
  /* empty */
  { Skip }
| block_item_list
  { $1 }
;

block_item_list:
  com block_item_list
  { Seq($1, $2) }
| com
  { $1 }
;

com:
  SEMICOLON
  { Skip }
| LBRACE block_item_list_opt RBRACE
  { $2 }
| IDENTIFIER EQ expr SEMICOLON
  { Assign($1, $3) }
| IF LPAREN expr RPAREN com %prec IF /* an ELSE should close the inner IF; shift when possible */
  { CondC($3, $5, Skip) }
| IF LPAREN expr RPAREN com ELSE com
  { CondC($3, $5, $7) }
| WHILE LPAREN expr RPAREN com
  { Loop(CondC($3, $5, Exit)) }
| IDENTIFIER LPAREN param_comma_list_opt RPAREN SEMICOLON
  { CallC($1, $3) }
| RETURN expr SEMICOLON
  { Return($2) }
;

param_comma_list_opt:
  /* empty */
  { [] }
| expr
  { [$1] }
| expr COMMA param_comma_list_opt
  { $1::$3 }
;

expr:
  STRINGCONSTANT
  { Const(StringV $1) }
| BCONSTANT
  { Const(BoolV $1) }
| INTCONSTANT
  { Const(IntV $1) }
| FLOATCONSTANT
  { Const(FloatV $1) }
| IDENTIFIER
  { VarE($1) }
| LPAREN expr RPAREN
  { $2 }
| expr QMARK expr COLON expr
  { CondE($1, $3, $5) }
| IDENTIFIER LPAREN param_comma_list_opt RPAREN
  { CallE($1, $3) }
| MINUS expr
  { BinOp(BInt BIsub, Const(IntV 0), $2) }
| FMINUS expr
  { BinOp(BFloat BFsub, Const(FloatV 0.), $2) }
| NOT expr
  { CondE($2, Const(BoolV false), Const(BoolV true)) }
| expr TIMES expr
  { BinOp(BInt BImul, $1, $3) }
| expr FTIMES expr
  { BinOp(BFloat BFmul, $1, $3) }
| expr DIV expr
  { BinOp(BInt BIdiv, $1, $3) }
| expr FDIV expr
  { BinOp(BFloat BFdiv, $1, $3) }
| expr MOD expr
  { BinOp(BInt BImod, $1, $3) }
| expr PLUS expr
  { BinOp(BInt BIadd, $1, $3) }
| expr FPLUS expr
  { BinOp(BFloat BFadd, $1, $3) }
| expr MINUS expr
  { BinOp(BInt BIsub, $1, $3) }
| expr FMINUS expr
  { BinOp(BFloat BFsub, $1, $3) }
| expr BLAND expr
  { BinOp(BBool BBand, $1, $3) }
| expr BLOR expr
  { BinOp(BBool BBor, $1, $3) }
| expr BCEQ expr
  { BinOp(BCompar BCeq, $1, $3) }
| expr BCNE expr
  { BinOp(BCompar BCne, $1, $3) }
| expr BCLT expr
  { BinOp(BCompar BClt, $1, $3) }
| expr BCLE expr
  { BinOp(BCompar BCle, $1, $3) }
| expr BCGT expr
  { BinOp(BCompar BCgt, $1, $3) }
| expr BCGE expr
  { BinOp(BCompar BCge, $1, $3) }
;
