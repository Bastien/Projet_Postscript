(* Compilation functions *)

open Lang
open Instrs

(* ************************************************************ *)
(* **** Compilation of expressions / statements            **** *)
(* ************************************************************ *)

let op_name = function
    BInt BIadd | BFloat BFadd -> "add" | BInt BIsub | BFloat BFsub -> "sub" | BInt BImul | BFloat BFmul -> "mul"
    | BInt BIdiv -> "idiv" | BFloat BFdiv -> "div" | BInt BImod -> "mod"
    | BBool BBand -> "and" | BBool BBor -> "or"
    | BCompar BCeq -> "eq" | BCompar BCge -> "ge" | BCompar BCgt -> "gt"
    | BCompar BCle -> "le" | BCompar BClt -> "lt" | BCompar BCne -> "ne"

let rec gen_expr = function
    Const v -> IVal v
  | VarE v -> IVar v
  | BinOp(op, e1, e2) -> ISeq [gen_expr e1; gen_expr e2; IOper(op_name op)]
  | CondE(c, e1, e2) -> ISeq [gen_expr c; IBloc(gen_expr e1); IBloc(gen_expr e2); IOper "ifelse"]
  | CallE(f, l) -> ISeq [ISeq(List.map gen_expr l); IOper(f)]

let is_void f =
  let rec loop = function
      Fundecl(tp, n, _)::_ when n = f -> tp = VoidT
    | _::r -> loop r
    | [] -> failwith "is_void: This error should never happen"
  in loop

let gen_com = function
    env -> let rec gen_com = function
               Skip -> ISeq []
             | Exit -> IOper "exit"
             | Assign(v, e) -> ISeq [IVal(LitV v); gen_expr e; IOper "def"]
             | Seq(c1, c2) -> ISeq [gen_com c1; gen_com c2]
             | CondC(c, c1, c2) -> ISeq [gen_expr c; IBloc(gen_com c1); IBloc(gen_com c2); IOper "ifelse"]
             | Loop c -> ILoop(gen_com c)
             | CallC(f, l) -> ISeq [ISeq(List.map gen_expr l); IOper(f); if is_void f env then ISeq [] else IOper "pop"]
             | Return e -> gen_expr e
           in gen_com

let gen_fun env (Fundefn(Fundecl(_, name, vardecls), com)) =
  IDef(name, ISeq [IVal(IntV(2 * List.length vardecls)); IOper "dict"; IOper "begin";
                   ISeq(List.rev_map (fun(Vardecl(_, n)) -> ISeq [IVal(LitV n); IOper "exch"; IOper "def"]) vardecls);
                   gen_com env com; IOper "end"])

let gen_prog (Prog(fundecls, fundefns)) =
  ISeq [ISeq(List.map (gen_fun fundecls) fundefns); IOper "main"]
